#!/bin/bash

ROOT="$(pwd)"
TEMP_DIR="$ROOT/tmp"
BUILD_BINUTILS="$ROOT/build_binutils"
BUILD_GCC="$ROOT/build_gcc"

TARGET=i686-elf
PREFIX="$TEMP_DIR/cross"

BINUTILS_VERSION=2.28
GCC_VERSION=7.1.0
BINUTILS_URL="http://ftp.gnu.org/gnu/binutils/binutils-$BINUTILS_VERSION.tar.gz"
GCC_URL="http://ftp.gnu.org/gnu/gcc/gcc-$GCC_VERSION/gcc-$GCC_VERSION.tar.gz"

BUILD_GCC="$TEMP_DIR/build-gcc"
BUILD_BINUTILS="$TEMP_DIR/build-binutils"

mkdir -p $TEMP_DIR
mkdir -p $PREFIX
mkdir -p $SYSROOT

configure_binutils () {
    cd $TEMP_DIR
    if [[ ! -d $BUILD_BINUTILS ]]; then
        if [[ ! -f "binutils-$BINUTILS_VERSION.tar.gz" ]]; then
            echo "DOWNLOAD BINUTILS"
            wget $BINUTILS_URL
        fi
        mkdir -p $BUILD_BINUTILS
        tar -C $TEMP_DIR -xvf "binutils-$BINUTILS_VERSION.tar.gz"
        cd $BUILD_BINUTILS
        ../binutils-$BINUTILS_VERSION/configure --target=$TARGET --prefix=$PREFIX --with-sysroot --disable-nls --disable-werror
        make
        make install 
    fi
}

configure_gcc () {
    cd $TEMP_DIR
    if [[ ! -d $BUILD_GCC ]]; then
        if [ ! -f "gcc-$GCC_VERSION.tar.gz" ]; then
            echo "DOWNLOADING GCC"
            wget $GCC_URL
        fi

        mkdir -p $BUILD_GCC
        tar -C $TEMP_DIR -xvf "gcc-$GCC_VERSION.tar.gz"
        cd $BUILD_GCC

        which -- $TARGET-as || echo "ERRO NO PATH DO $TARGET-as"

        ../gcc-$GCC_VERSION/configure --prefix=$PREFIX --target=$TARGET --disable-nls --enable-languages=c,c++ --without-headers
        make all-gcc
        make all-target-libgcc
        make install-gcc
        make install-target-libgcc
    fi

}

configure_binutils
configure_gcc