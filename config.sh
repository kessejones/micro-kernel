#!/bin/bash

### Config environment

#### Variables

HEADERS="libc kernel"
PROJECTS="libc kernel"

HOST=i686-elf

#### PATHs
export ROOT=$(pwd)
export TEMP_DIR="$ROOT/tmp"
export SYSROOT="$ROOT/sysroot"
export CROSS="$TEMP_DIR/cross"
export PATH="$CROSS/bin:$PATH"

export USR_DIR="/usr"
export INCLUDE_DIR="$USR_DIR/include"
export LIB_DIR="$USR_DIR/lib"
export KERNEL_DIR="$ROOT/kernel"
export LIBC_DIR="$ROOT/libc"
export BUILD_DIR="$TEMP_DIR/build-kernel"

#### Compiler names
export AS="$HOST-as"
export CC="$HOST-gcc --sysroot=$SYSROOT -isystem=$INCLUDE_DIR"
export AR="$HOST-ar"

set -e