#ifndef _KERNEL_IO_IO_H
#define _KERNEL_IO_IO_H

void outportb(unsigned short _port, unsigned char _data);

unsigned char inportb (unsigned short _port);

#endif