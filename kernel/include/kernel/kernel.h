#ifndef _KERNEL_KERNEL_H
#define _KERNEL_KERNEL_H

#include <kernel/video/tty.h>
#include <kernel/memory/gdt.h>
#include <kernel/cpu/idt.h>
#include <kernel/cpu/isr.h>
#include <kernel/cpu/irq.h>
#include <kernel/cpu/timer.h>
#include <kernel/keyboard/keyboard.h>

#ifdef __cplusplus
extern "C" {
#endif

void kernel_main(void);

#ifdef __cplusplus
}
#endif

#endif