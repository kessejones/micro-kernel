#ifndef _KERNEL_CPU_TIMER_H
#define _KERNEL_CPU_TIMER_H

#include <kernel/system.h>

void timer_handler(struct regs* r);

void timer_install();

void timer_wait(int ticks);

#endif