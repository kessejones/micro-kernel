#ifndef _KERNEL_MEMORY_GDT_H
#define _KERNEL_MEMORY_GDT_H 1

struct gdt_entry {
    unsigned short limit_low;
    unsigned short base_low;
    unsigned char base_middle;
    unsigned char access;
    unsigned char granularity;
    unsigned char base_high;
} __attribute__ ((packed));

struct gdt_ptr {
    unsigned short limit;
    unsigned int base;
} __attribute__ ((packed));

struct gdt_entry gdt[3];
struct gdt_ptr _gp;

void gdt_set_gate(int num, unsigned long base, unsigned long limit, unsigned char access, unsigned char gran);
void gdt_install();

extern void _gdt_flush();

#endif