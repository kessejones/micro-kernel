#include <kernel/cpu/idt.h>
#include <string.h>

void idt_set_gate(unsigned char num, unsigned long base, unsigned short sel, unsigned char flags) {
    idt[num].base_lo = (base & 0xFFFF);
    idt[num].base_hi = (base >> 16) & 0xFFFF;

    idt[num].sel = sel;
    idt[num].flags = flags;
    idt[num].always0 = 0; 
}

void idt_install() {
    _idtp.limit = (sizeof(struct idt_entry) * 256) -1;
    _idtp.base = &idt;

    memset(&idt, 0, sizeof(struct idt_entry) * 256);
    _idt_load();
}