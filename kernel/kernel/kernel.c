#include <kernel/kernel.h>
#include <stdio.h>
  
void kernel_main(void) {
	terminal_initialiaze();

	printf("Starting GDT...");
	gdt_install();
	printf("[OK]\n");

	printf("Starting IDT...");
	idt_install();
	printf("[OK]\n");
	
	printf("Starting ISR...");
	isrs_install();
	printf("[OK]\n");

	printf("Starting IRQ...");
	irq_install();
	printf("[OK]\n");
	
	printf("Starting Timer...");
	timer_install();
	printf("[OK]\n");
	
	printf("Starting Keyboard...");
	keyboard_install();
	printf("[OK]\n");

	
	int a = 10 / 0;
	// for(int i = 0; i < 10; i++)
		// printf("Hello, kernel World! %c \n", i+'0');
	// printf("Hello, kernel World! \n");
	// printf("Hello, kernel World! %c\n", 1 / 0);

	for (;;);
}