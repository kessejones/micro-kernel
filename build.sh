#!/bin/bash

### Run variable configuration script

bash ./env.sh

. ./config.sh
. ./clean.sh


### Install headers
mkdir -p $SYSROOT
mkdir -p $SYSROOT/boot
mkdir -p $SYSROOT/boot/grub

mkdir -p $BUILD_DIR

mkdir -p $SYSROOT$INCLUDE_DIR

mkdir -p $SYSROOT$LIB_DIR

mkdir -p $LIBC_DIR/objs

for PROJECT in $HEADERS; do
    (cd $PROJECT && make install-headers)
done

### Compile Libc
for PROJECT in $PROJECTS; do
    echo "\n\n$PROJECT\n"
    (cd $PROJECT && make all)
done


cd $ROOT
if grub-file --is-x86-multiboot $BUILD_DIR/my_kernel.bin; then
    echo multiboot confirmed
else
    echo the file is not multiboot
fi

grub-mkrescue -o my_kernel.iso $SYSROOT